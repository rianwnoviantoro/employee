package com.example.spring.employee.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("")
public class AuthController {

    @GetMapping(value = "/login")
    public ModelAndView Login()
    {
        ModelAndView view = new ModelAndView("auth/login");
        return view;
    }

    @GetMapping(value = "/register")
    public ModelAndView Register()
    {
        ModelAndView view = new ModelAndView("auth/register");
        return view;
    }

}
