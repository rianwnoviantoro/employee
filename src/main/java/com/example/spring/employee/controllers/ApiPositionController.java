package com.example.spring.employee.controllers;

import com.example.spring.employee.models.Position;
import com.example.spring.employee.models.Role;
import com.example.spring.employee.repositories.PositionRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class ApiPositionController {

    @Autowired
    private PositionRepo positionRepo;

    @GetMapping("/positions")
    public ResponseEntity<List<Position>> GetAllRole()
    {
        try
        {
            List<Position> position = this.positionRepo.findAll();
            return new ResponseEntity<>(position, HttpStatus.OK);
        }

        catch (Exception e)
        {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping("/position")
    public ResponseEntity<Object> SavePosition(@RequestBody Position position)
    {
        try {
            this.positionRepo.save(position);
            return new ResponseEntity<>(position, HttpStatus.CREATED);
        }

        catch (Exception e)
        {
            return new ResponseEntity<>("Failed", HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/position/{id}")
    public ResponseEntity<List<Role>> GetPositionById(@PathVariable("id") Long id)
    {
        try {
            Optional<Position> position = this.positionRepo.findById(id);

            if (position.isPresent())
            {
                ResponseEntity rest = new ResponseEntity<>(position, HttpStatus.OK);
                return rest;
            } else {
                return ResponseEntity.notFound().build();
            }
        }

        catch (Exception e)
        {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("/searchposition/{keyword}")
    public ResponseEntity<List<Position>> SearchPosition(@PathVariable("keyword") String keyword)
    {
        if (keyword != null)
        {
            List<Position> position = this.positionRepo.SearchPosition(keyword);
            return new ResponseEntity<>(position, HttpStatus.OK);
        } else {
            List<Position> position = this.positionRepo.findAll();
            return new ResponseEntity<>(position, HttpStatus.OK);
        }
    }

    @GetMapping("/positionmapped")
    public ResponseEntity<Map<String, Object>> GetAllPage(@RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "5") int size)
    {
        try {
            List<Position> position = new ArrayList<>();
            Pageable pagingSort = PageRequest.of(page, size);

            Page<Position> pageTuts;

            pageTuts = positionRepo.findAll(pagingSort);

            position = pageTuts.getContent();

            Map<String, Object> response = new HashMap<>();
            response.put("position", position);
            response.put("currentPage", pageTuts.getNumber());
            response.put("totalItems", pageTuts.getTotalElements());
            response.put("totalPages", pageTuts.getTotalPages());

            return new ResponseEntity<>(response, HttpStatus.OK);
        }

        catch (Exception e)
        {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/position/{id}")
    public ResponseEntity<List<Position>> UpdatePosition(@PathVariable("id") Long id, @RequestBody Position position)
    {
        try {
            Optional<Position> positionData = this.positionRepo.findById(id);

            if (positionData.isPresent())
            {
                position.setId(id);
                this.positionRepo.save(position);
                ResponseEntity rest = new ResponseEntity<>(position, HttpStatus.CREATED);
                return rest;
            } else {
                return ResponseEntity.notFound().build();
            }
        }

        catch (Exception e)
        {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @DeleteMapping("/position/{id}")
    public ResponseEntity<Object> DeletePosition(@PathVariable("id") Long id)
    {
        this.positionRepo.deleteById(id);
        return new ResponseEntity<>("Success", HttpStatus.CREATED);
    }

}
