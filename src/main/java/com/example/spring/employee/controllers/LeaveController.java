package com.example.spring.employee.controllers;

import com.example.spring.employee.repositories.LeaveRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/leave")
public class LeaveController {

    @Autowired
    private LeaveRepo leaveRepo;

    @GetMapping(value = "")
    public ModelAndView index()
    {
        ModelAndView view = new ModelAndView("leave/index");
        return view;
    }

    @GetMapping(value = "/request")
    public ModelAndView request()
    {
        ModelAndView view = new ModelAndView("leave/newLeave");
        return view;
    }

    @GetMapping(value = "/edit/{id}")
    public ModelAndView editLeave()
    {
        ModelAndView view = new ModelAndView("leave/editLeave");
        return view;
    }
}
