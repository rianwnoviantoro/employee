package com.example.spring.employee.controllers;

import com.example.spring.employee.models.Employee;
import com.example.spring.employee.models.Leave;
import com.example.spring.employee.models.Role;
import com.example.spring.employee.repositories.LeaveRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class ApiLeaveController {

    @Autowired
    private LeaveRepo leaveRepo;

    @GetMapping("/leaves")
    public ResponseEntity<List<Leave>> GetLeaveHistory()
    {
        try
        {
            List<Leave> role = this.leaveRepo.findAll();
            return new ResponseEntity<>(role, HttpStatus.OK);
        }

        catch (Exception e)
        {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("/leave/{id}")
    public ResponseEntity<List<Leave>> GetLeaveById(@PathVariable("id") Long id)
    {
        try {
            Optional<Leave> leave = this.leaveRepo.findById(id);

            if (leave.isPresent())
            {
                ResponseEntity rest = new ResponseEntity<>(leave, HttpStatus.OK);
                return rest;
            } else {
                return ResponseEntity.notFound().build();
            }
        }

        catch (Exception e)
        {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("/searchleave/{keyword}")
    public ResponseEntity<List<Leave>> SearchLeave(@PathVariable("keyword") String keyword)
    {
        if (keyword != null)
        {
            List<Leave> leave = this.leaveRepo.SearchLeave(keyword);
            return new ResponseEntity<>(leave, HttpStatus.OK);
        } else {
            List<Leave> leave = this.leaveRepo.findAll();
            return new ResponseEntity<>(leave, HttpStatus.OK);
        }
    }

    @GetMapping("/leavemapped")
    public ResponseEntity<Map<String, Object>> GetAllPage(@RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "5") int size)
    {
        try {
            List<Leave> leave = new ArrayList<>();
            Pageable pagingSort = PageRequest.of(page, size);

            Page<Leave> pageTuts;

            pageTuts = leaveRepo.findAll(pagingSort);

            leave = pageTuts.getContent();

            Map<String, Object> response = new HashMap<>();
            response.put("leave", leave);
            response.put("currentPage", pageTuts.getNumber());
            response.put("totalItems", pageTuts.getTotalElements());
            response.put("totalPages", pageTuts.getTotalPages());

            return new ResponseEntity<>(response, HttpStatus.OK);
        }

        catch (Exception e)
        {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/leave")
    public ResponseEntity<Object> SaveLeave(@RequestBody Leave leave)
    {
        try {
            this.leaveRepo.save(leave);
            return new ResponseEntity<>(leave, HttpStatus.CREATED);
        }

        catch (Exception e)
        {
            return new ResponseEntity<>("Failed", HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("/leave/{id}")
    public ResponseEntity<List<Leave>> UpdateLeave(@PathVariable("id") Long id, @RequestBody Leave leave)
    {
        try {
            Optional<Leave> leaveData = this.leaveRepo.findById(id);

            if (leaveData.isPresent())
            {
                leave.setId(id);
                this.leaveRepo.save(leave);
                ResponseEntity rest = new ResponseEntity<>(leave, HttpStatus.CREATED);
                return rest;
            } else {
                return ResponseEntity.notFound().build();
            }
        }

        catch (Exception e)
        {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @DeleteMapping("/leave/{id}")
    public ResponseEntity<Object> DeleteLeave(@PathVariable("id") Long id)
    {
        this.leaveRepo.deleteById(id);
        return new ResponseEntity<>("Success", HttpStatus.CREATED);
    }

}
