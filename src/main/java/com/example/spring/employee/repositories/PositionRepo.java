package com.example.spring.employee.repositories;

import com.example.spring.employee.models.Position;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface PositionRepo extends JpaRepository<Position, Long> {
    @Query("FROM Position WHERE lower(Name) LIKE lower(concat('%',?1,'%')) OR lower(PositionCode) LIKE lower(concat('%',?1,'%'))")
    List<Position> SearchPosition(String keyword);
}
