package com.example.spring.employee.repositories;

import com.example.spring.employee.models.Auth;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AuthRepo extends JpaRepository<Auth, Long> {

}
