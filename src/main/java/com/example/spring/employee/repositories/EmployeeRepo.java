package com.example.spring.employee.repositories;

import com.example.spring.employee.models.Employee;
import com.example.spring.employee.models.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface EmployeeRepo extends JpaRepository<Employee, Long> {
    @Query("FROM Employee WHERE lower(Nip) LIKE lower(concat('%',?1,'%')) OR lower(Name) LIKE lower(concat('%',?1,'%')) OR lower(Nohp) LIKE lower(concat('%',?1,'%')) OR lower(Email) LIKE lower(concat('%',?1,'%')) OR lower(Pob) LIKE lower(concat('%',?1,'%')) OR lower(Religion) LIKE lower(concat('%',?1,'%'))" )
    List<Employee> SearchEmployee(String keyword);
}
