package com.example.spring.employee.controllers;

import com.example.spring.employee.repositories.PositionRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/position")
public class PositionController {

    @Autowired
    private PositionRepo positionRepo;

    @GetMapping(value = "")
    public ModelAndView index()
    {
        ModelAndView view = new ModelAndView("position/index");
        return view;
    }

    @GetMapping(value = "/new")
    public ModelAndView newPosition()
    {
        ModelAndView view = new ModelAndView("position/newPosition");
        return view;
    }

    @GetMapping(value = "/edit/{id}")
    public ModelAndView editPosition()
    {
        ModelAndView view = new ModelAndView("position/editPosition");
        return view;
    }
}
