package com.example.spring.employee.repositories;

import com.example.spring.employee.models.Position;
import com.example.spring.employee.models.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface RoleRepo extends JpaRepository<Role, Long> {
    @Query("FROM Role WHERE lower(Name) LIKE lower(concat('%',?1,'%')) OR lower(RoleCode) LIKE lower(concat('%',?1,'%'))")
    List<Role> SearchRole(String keyword);
}
