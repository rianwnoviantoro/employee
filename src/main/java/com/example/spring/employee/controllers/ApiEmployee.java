package com.example.spring.employee.controllers;

import com.example.spring.employee.models.Employee;
import com.example.spring.employee.repositories.EmployeeRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class ApiEmployee {

    @Autowired
    private EmployeeRepo employeeRepo;

    @GetMapping("/employees")
    public ResponseEntity<List<Employee>> GetAllEmployee()
    {
        try
        {
            List<Employee> employee = this.employeeRepo.findAll();
            return new ResponseEntity<>(employee, HttpStatus.OK);
        }

        catch (Exception e)
        {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("/employee/{id}")
    public ResponseEntity<List<Employee>> GetEmployeeById(@PathVariable("id") Long id)
    {
        try {
            Optional<Employee> employee = this.employeeRepo.findById(id);

            if (employee.isPresent())
            {
                ResponseEntity rest = new ResponseEntity<>(employee, HttpStatus.OK);
                return rest;
            } else {
                return ResponseEntity.notFound().build();
            }
        }

        catch (Exception e)
        {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("/searchemployee/{keyword}")
    public ResponseEntity<List<Employee>> SearchEmployee(@PathVariable("keyword") String keyword)
    {
        if (keyword != null)
        {
            List<Employee> employee = this.employeeRepo.SearchEmployee(keyword);
            return new ResponseEntity<>(employee, HttpStatus.OK);
        } else {
            List<Employee> employee = this.employeeRepo.findAll();
            return new ResponseEntity<>(employee, HttpStatus.OK);
        }
    }

    @GetMapping("/employeemapped")
    public ResponseEntity<Map<String, Object>> GetAllPage(@RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "5") int size)
    {
        try {
            List<Employee> employee = new ArrayList<>();
            Pageable pagingSort = PageRequest.of(page, size);

            Page<Employee> pageTuts;

            pageTuts = employeeRepo.findAll(pagingSort);

            employee = pageTuts.getContent();

            Map<String, Object> response = new HashMap<>();
            response.put("employee", employee);
            response.put("currentPage", pageTuts.getNumber());
            response.put("totalItems", pageTuts.getTotalElements());
            response.put("totalPages", pageTuts.getTotalPages());

            return new ResponseEntity<>(response, HttpStatus.OK);
        }

        catch (Exception e)
        {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/employee")
    public ResponseEntity<Object> SaveEmployee(@RequestBody Employee employee)
    {
        try {
            this.employeeRepo.save(employee);
            return new ResponseEntity<>(employee, HttpStatus.CREATED);
        }

        catch (Exception e)
        {
            return new ResponseEntity<>("Failed", HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("/employee/{id}")
    public ResponseEntity<List<Employee>> UpdateEmployee(@PathVariable("id") Long id, @RequestBody Employee employee)
    {
        try {
            Optional<Employee> employeeData = this.employeeRepo.findById(id);

            if (employeeData.isPresent())
            {
                employee.setId(id);
                this.employeeRepo.save(employee);
                ResponseEntity rest = new ResponseEntity<>(employee, HttpStatus.CREATED);
                return rest;
            } else {
                return ResponseEntity.notFound().build();
            }
        }

        catch (Exception e)
        {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @DeleteMapping("/employee/{id}")
    public ResponseEntity<Object> DeleteEmployee(@PathVariable("id") Long id)
    {
        this.employeeRepo.deleteById(id);
        return new ResponseEntity<>("Success", HttpStatus.CREATED);
    }

}
