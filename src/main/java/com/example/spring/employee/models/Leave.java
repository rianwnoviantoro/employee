package com.example.spring.employee.models;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "leaves")
public class Leave {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long Id;

    @ManyToOne
    @JoinColumn(name = "employee_id", insertable = false, updatable = false)
    public Employee employee;

    @Column(name = "employee_id", nullable = false)
    private long EmployeeId;

    @Column(name = "leave_type", nullable = false)
    private String LeaveType;

    @Column(name = "start_date", nullable = false)
    private Date StartDate;

    @Column(name = "end_date", nullable = false)
    private Date EndDate;

    @Column(name = "duration", nullable = false)
    private String Duration;

    @Column(name = "reason", nullable = false)
    private String Reason;

    public long getId() {
        return Id;
    }

    public void setId(long id) {
        Id = id;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public long getEmployeeId() {
        return EmployeeId;
    }

    public void setEmployeeId(long employeeId) {
        EmployeeId = employeeId;
    }

    public String getLeaveType() {
        return LeaveType;
    }

    public void setLeaveType(String leaveType) {
        LeaveType = leaveType;
    }

    public Date getStartDate() {
        return StartDate;
    }

    public void setStartDate(Date startDate) {
        StartDate = startDate;
    }

    public Date getEndDate() {
        return EndDate;
    }

    public void setEndDate(Date endDate) {
        EndDate = endDate;
    }

    public String getDuration() {
        return Duration;
    }

    public void setDuration(String duration) {
        Duration = duration;
    }

    public String getReason() {
        return Reason;
    }

    public void setReason(String reason) {
        Reason = reason;
    }
}
