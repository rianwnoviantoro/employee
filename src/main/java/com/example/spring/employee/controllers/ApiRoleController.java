package com.example.spring.employee.controllers;

import com.example.spring.employee.models.Position;
import com.example.spring.employee.models.Role;
import com.example.spring.employee.repositories.RoleRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class ApiRoleController {

    @Autowired
    private RoleRepo roleRepo;

    @GetMapping("/roles")
    public ResponseEntity<List<Role>> GetAllRole()
    {
        try
        {
            List<Role> role = this.roleRepo.findAll();
            return new ResponseEntity<>(role, HttpStatus.OK);
        }

        catch (Exception e)
        {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("/role/{id}")
    public ResponseEntity<List<Role>> GetRoleById(@PathVariable("id") Long id)
    {
        try {
            Optional<Role> role = this.roleRepo.findById(id);

            if (role.isPresent())
            {
                ResponseEntity rest = new ResponseEntity<>(role, HttpStatus.OK);
                return rest;
            } else {
                return ResponseEntity.notFound().build();
            }
        }

        catch (Exception e)
        {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("/searchrole/{keyword}")
    public ResponseEntity<List<Role>> SearchRole(@PathVariable("keyword") String keyword)
    {
        if (keyword != null)
        {
            List<Role> role = this.roleRepo.SearchRole(keyword);
            return new ResponseEntity<>(role, HttpStatus.OK);
        } else {
            List<Role> role = this.roleRepo.findAll();
            return new ResponseEntity<>(role, HttpStatus.OK);
        }
    }

    @GetMapping("/rolemapped")
    public ResponseEntity<Map<String, Object>> GetAllPage(@RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "5") int size)
    {
        try {
            List<Role> role = new ArrayList<>();
            Pageable pagingSort = PageRequest.of(page, size);

            Page<Role> pageTuts;

            pageTuts = roleRepo.findAll(pagingSort);

            role = pageTuts.getContent();

            Map<String, Object> response = new HashMap<>();
            response.put("role", role);
            response.put("currentPage", pageTuts.getNumber());
            response.put("totalItems", pageTuts.getTotalElements());
            response.put("totalPages", pageTuts.getTotalPages());

            return new ResponseEntity<>(response, HttpStatus.OK);
        }

        catch (Exception e)
        {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/role")
    public ResponseEntity<Object> SaveRole(@RequestBody Role role)
    {
        try {
            this.roleRepo.save(role);
            return new ResponseEntity<>(role, HttpStatus.CREATED);
        }

        catch (Exception e)
        {
            return new ResponseEntity<>("Failed", HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("/role/{id}")
    public ResponseEntity<List<Role>> UpdateRole(@PathVariable("id") Long id, @RequestBody Role role)
    {
        try {
            Optional<Role> roleData = this.roleRepo.findById(id);

            if (roleData.isPresent())
            {
                role.setId(id);
                this.roleRepo.save(role);
                ResponseEntity rest = new ResponseEntity<>(role, HttpStatus.CREATED);
                return rest;
            } else {
                return ResponseEntity.notFound().build();
            }
        }

        catch (Exception e)
        {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @DeleteMapping("/role/{id}")
    public ResponseEntity<Object> DeleteRole(@PathVariable("id") Long id)
    {
        this.roleRepo.deleteById(id);
        return new ResponseEntity<>("Success", HttpStatus.CREATED);
    }
}
