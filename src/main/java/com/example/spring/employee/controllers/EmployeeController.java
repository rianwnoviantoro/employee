package com.example.spring.employee.controllers;

import com.example.spring.employee.repositories.EmployeeRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/employee")
public class EmployeeController {

    @Autowired
    private EmployeeRepo employeeRepo;

    @GetMapping(value = "")
    public ModelAndView index()
    {
        ModelAndView view = new ModelAndView("employee/index");
        return view;
    }

    @GetMapping(value = "/new")
    public ModelAndView newEmployee()
    {
        ModelAndView view = new ModelAndView("employee/newEmployee");
        return view;
    }

    @GetMapping(value = "/edit/{id}")
    public ModelAndView editEmployee()
    {
        ModelAndView view = new ModelAndView("employee/editEmployee");
        return view;
    }

}
