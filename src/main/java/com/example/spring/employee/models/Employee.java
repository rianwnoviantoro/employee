package com.example.spring.employee.models;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "employees")
public class Employee {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long Id;

    @Column(name = "nip", nullable = false)
    private String Nip;

    @Column(name = "name", nullable = false)
    private String Name;

    @Column(name = "dob", nullable = false)
    private Date Dob;

    @Column(name = "pob", nullable = false)
    private String Pob;

    @Column(name = "nohp", nullable = false)
    private String Nohp;

    @Column(name = "email", nullable = false)
    private String Email;

    @OneToOne
    @JoinColumn(name = "position_id", insertable = false, updatable = false)
    public Position position;

    @Column(name = "position_id", nullable = true)
    private long PositionId;

    @OneToOne
    @JoinColumn(name = "role_id", insertable = false, updatable = false)
    public Role role;

    @Column(name = "role_id", nullable = true)
    private long RoleId;

    @Column(name = "religion", nullable = false)
    private String Religion;

    @Column(name = "leave_remaining", nullable = false)
    private long LeaveRemaining = 12 ;

    public long getId() {
        return Id;
    }

    public void setId(long id) {
        Id = id;
    }

    public String getNip() {
        return Nip;
    }

    public void setNip(String nip) {
        Nip = nip;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public Date getDob() {
        return Dob;
    }

    public void setDob(Date dob) {
        Dob = dob;
    }

    public String getPob() {
        return Pob;
    }

    public void setPob(String pob) {
        Pob = pob;
    }

    public String getNohp() {
        return Nohp;
    }

    public void setNohp(String nohp) {
        Nohp = nohp;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public long getPositionId() {
        return PositionId;
    }

    public void setPositionId(long positionId) {
        PositionId = positionId;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public long getRoleId() {
        return RoleId;
    }

    public void setRoleId(long roleId) {
        RoleId = roleId;
    }

    public String getReligion() {
        return Religion;
    }

    public void setReligion(String religion) {
        Religion = religion;
    }

    public long getLeaveRemaining() {
        return LeaveRemaining;
    }

    public void setLeaveRemaining(long leaveRemaining) {
        LeaveRemaining = leaveRemaining;
    }
}
