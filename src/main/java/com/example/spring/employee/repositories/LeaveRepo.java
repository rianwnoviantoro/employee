package com.example.spring.employee.repositories;

import com.example.spring.employee.models.Leave;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface LeaveRepo extends JpaRepository<Leave, Long> {
    @Query("FROM Leave l INNER JOIN Employee e ON l.EmployeeId = e.Id WHERE lower(l.LeaveType) LIKE lower(concat('%',?1,'%')) OR lower(l.Duration) LIKE lower(concat('%',?1,'%')) OR lower(e.Nip) LIKE lower(concat('%',?1,'%')) OR lower(e.Name) LIKE lower(concat('%',?1,'%'))" )
    List<Leave> SearchLeave(String keyword);
}
