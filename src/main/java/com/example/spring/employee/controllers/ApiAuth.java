package com.example.spring.employee.controllers;

import com.example.spring.employee.models.Auth;
import com.example.spring.employee.repositories.AuthRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class ApiAuth {

    @Autowired
    private AuthRepo authRepo;

    @PostMapping("/register")
    public ResponseEntity<Object> SaveUser(@RequestBody Auth auth)
    {
        String password = auth.getPassword();

        try {
            return new ResponseEntity<>(auth, HttpStatus.OK);
        }

        catch (Exception e)
        {
            return new ResponseEntity<>("Failed", HttpStatus.BAD_REQUEST);
        }
    }

}
