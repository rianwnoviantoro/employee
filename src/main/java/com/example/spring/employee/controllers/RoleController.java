package com.example.spring.employee.controllers;

import com.example.spring.employee.repositories.RoleRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(value = "/role")
public class RoleController {

    @Autowired
    private RoleRepo roleRepo;

    @GetMapping(value = "")
    public ModelAndView index()
    {
        ModelAndView view = new ModelAndView("role/index");
        return view;
    }

    @GetMapping(value = "/new")
    public ModelAndView newRole()
    {
        ModelAndView view = new ModelAndView("role/newRole");
        return view;
    }

    @GetMapping(value = "/edit/{id}")
    public ModelAndView editRole()
    {
        ModelAndView view = new ModelAndView("role/editRole");
        return view;
    }
}
