package com.example.spring.employee.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("")
public class HomeController {

    @GetMapping(value = "")
    public ModelAndView Login()
    {
        ModelAndView view = new ModelAndView("home/index");
        return view;
    }

}
